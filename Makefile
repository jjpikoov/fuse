CC	=	/usr/bin/gcc
CFLAGS	=	-g -Wall $(PKGFLAGS) 
PKGFLAGS = `pkg-config fuse3 --cflags --libs`

build: paw16_ll.c
	$(CC) $(CFLAGS) paw16_ll.c -o paw16
clean:
	rm *.o paw_16