#define FUSE_USE_VERSION 30
#define BUFF_SIZE 512
#define DEBUGG @@@@@@@@@@@@@
#define NAME_SIZE 13

#define _GNU_SOURCE

#include "config.h"

#include <fuse_lowlevel.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <syslog.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

#include "paw16_ll.h"

#define panic()                                                 \
  __extension__({                                               \
  fprintf(stderr, "%s: %s\n", __func__, strerror(errno));     \
  exit(EXIT_FAILURE);                                         \
  })

int blk_open(const char *path) {
    int fd = open(path, O_RDWR | O_DIRECT);
    
    if (fd < 0)
        panic();
    
    return fd;
}

void blk_close(int fd) {
    if (close(fd) < 0)
        panic();
}

void blk_read(int fd, void *buf, size_t blkoff, size_t blkcnt) {
    size_t offset = blkoff * BLKSIZE;
    size_t count = blkcnt * BLKSIZE;
    
    if (lseek(fd, offset, SEEK_SET) < 0)
        panic();
    
    ssize_t actual = read(fd, buf, count);
    
    if (actual < 0)
        panic();
    
    assert((size_t)actual == count);
}

void blk_write(int fd, const void *buf, size_t blkoff, size_t blkcnt) {
    size_t offset = blkoff * BLKSIZE;
    size_t count = blkcnt * BLKSIZE;
    
    if (lseek(fd, offset, SEEK_SET) < 0)
        panic();
    
    ssize_t actual = write(fd, buf, count);
    
    if (actual < 0)
        panic();
    
    assert((size_t)actual == count);
}



int fat_offset;
int fd;
struct bpb50* bootb;
size_t sector_count;
size_t cluster_count;
struct direntry* root_dir_entries;
struct direntry* dir_entries;
int data_sector_offset;
int cluster_size, dir_entry_size;
char* readable_names;
char* root_names;
char* readable_root_names;
int dir_entr_count;

void fill_direntries_trigger() {
    for (int i = 0; i < bootb->bpb_root_dir_ents; i++) {
        if (root_dir_entries[i].name[0] != 0) {
            dir_entries[root_dir_entries[i].start_cluster] = root_dir_entries[i];
            fill_readable_name(root_dir_entries[i].name, readable_root_names[i]);
            dir_entr_count++;
            if (root_dir_entries[i].attributes & ATTR_DIRECTORY)
                fill_direntries(root_dir_entries[i].start_cluster + 1);
        }
    }
}

void fill_direntries(fuse_ino_t ino) {
    struct direntry tmp = dir_entries[ino - 1];
    char *current_cluster;
    current_cluster = malloc(bootb->bpb_sec_per_clust * bootb->bpb_bytes_per_sec);
    int clusterSectorOffset = data_sector_offset + (tmp.start_cluster * bootb->bpb_sec_per_clust);
    blk_read(fd, current_cluster, clusterSectorOffset, bootb->bpb_sec_per_clust);

    int currentClusterNo = tmp.start_cluster;
    int done = 0;

    while (!done) {
        for (size_t i = 0; i < cluster_size; i += dir_entry_size) {
            struct direntry current_entry;
            memcpy(&current_entry, current_cluster + i, dir_entry_size);

            if (current_entry.name[0] != 0x00 &&
                current_entry.name[0]!= '.' &&
                (u_int8_t)current_entry.name[0] != SLOT_DELETED) {
                
                dir_entries[current_entry.start_cluster] = current_entry;
                fill_readable_name(current_entry.name, readable_names[current_entry.start_cluster]);

                if (current_entry.attributes & ATTR_DIRECTORY)
                    fill_direntries(current_entry.start_cluster + 1);
            }
        }

        done = load_next_cluster(current_cluster, &currentClusterNo);
    }

    free(current_cluster);
}

time_t time_fat_to_unix(u_int16_t date, u_int16_t time) {
    time_t day_secs = 24 * 60 * 60;

    u_int16_t year = (date >> 9) + 10;
    u_int16_t month = (date & 0x01E0) >> 5;
    u_int16_t day = date & 0x001F;

    u_int16_t loop_yrs = 0;
    u_int16_t count_yr = 1972;
    u_int16_t actual_yr = year + 1970;
    
    while (count_yr < actual_yr) {
        count_yr += 4;
        loop_yrs++;
    }

    time_t years_off = (year * 365 * day_secs) + (loop_yrs * day_secs);

    u_int16_t days_passed_non_leap[14] = { 0, 0, 31, 59, 90, 121, 151, 182, 212, 243, 273, 304, 334, 365 };
    u_int16_t days_passed_leap[14] = { 0, 0, 31, 60, 91, 120, 152, 183, 213, 244, 274, 305, 335, 366 };

    time_t months_off = ((actual_yr & 3) == 0 && ((actual_yr % 25) != 0 || (actual_yr & 15) == 0)) ?
        days_passed_leap[month] * day_secs :
        days_passed_non_leap[month] * day_secs;

    time_t daysOff = (day - 1) * day_secs;
    time_t hoursOff = (time >> 11) * 60 * 60;
    time_t minutesOff = ((time & 0x08E0) >> 5) * 60;
    time_t secondsOff = (time & 0x001F) * 2;

    return years_off + months_off + daysOff + hoursOff + minutesOff + secondsOff;
}

void fill_readable_name(char *fat_name, char *readable_name) {
    int name_len, ext_len;
    
    for (name_len = 8; fat_name[name_len - 1] == ' ' && name_len > 0; name_len--) { }
    for (ext_len = 0; fat_name[ext_len + 8] != ' ' && ext_len < 3; ext_len++) { }
    
    for (int i = 0; i < name_len; i++)
        readable_name[i] = tolower(fat_name[i]);
        
    if (ext_len > 0) {
        readable_name[name_len] = '.';
        
        for (int i = 0; i < ext_len; i++)
            readable_name[i + name_len + 1] = tolower(fat_name[i + 8]);

        readable_name[name_len + ext_len + 2] = '\0';
    }
    else {
        readable_name[name_len] = '\0';
    }
}

int load_next_cluster(char* current_cluster, int* current_cluster_nb) {
    u_int16_t* fat_block = malloc(bootb->bpb_bytes_per_sec);
    int fat_entry_offset = fat_offset + (*current_cluster_nb * 2) + 1;
    
    blk_read(fd, fat_block, fat_entry_offset / (bootb->bpb_bytes_per_sec), 1);
    int next_cluster_nb = fat_block[(fat_entry_offset % bootb->bpb_bytes_per_sec) / sizeof(u_int16_t)];
    
    free(fat_block);

    if (next_cluster_nb < 0xFFF7) {
        *current_cluster_nb = next_cluster_nb;
        int clusterSectorOffset = data_sector_offset + (*current_cluster_nb * bootb->bpb_sec_per_clust);
        blk_read(fd, current_cluster, clusterSectorOffset, bootb->bpb_sec_per_clust);
        return 0;
    }
    else {
        return -1;
    }
}

#define min(x, y) ((x) < (y) ? (x) : (y))

int reply_buf_limited(fuse_req_t req, const char *buf, size_t bufsize, off_t off, size_t maxsize) {
    if (off < (ssize_t)bufsize)
        return fuse_reply_buf(req, buf + off, min(bufsize - off, maxsize));
    else
        return fuse_reply_buf(req, NULL, 0);
}

int paw16_stat(fuse_ino_t ino, struct stat *stbuf) {
    stbuf->st_ino = ino;
    stbuf->st_blksize = cluster_size;

    if (ino == 1) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    }
    else {
        struct direntry tmp = dir_entries[ino - 1];
        
        time_t unixTime = time_fat_to_unix(tmp.m_date, tmp.m_time);
        stbuf->st_atim.tv_sec = unixTime;
        stbuf->st_mtim.tv_sec = unixTime;
        stbuf->st_ctim.tv_sec = unixTime;

        if (tmp.attributes & ATTR_DIRECTORY) {
            stbuf->st_mode = S_IFDIR | 0755;
            stbuf->st_nlink = 2;
        } 
        else {
            stbuf->st_mode = S_IFREG | 0444;
            stbuf->st_nlink = 1;
            stbuf->st_size = tmp.file_size;
            stbuf->st_blocks = (tmp.file_size % bootb->bpb_bytes_per_sec == 0) ? 
                (tmp.file_size / bootb->bpb_bytes_per_sec) : 
                (tmp.file_size / bootb->bpb_bytes_per_sec) + 1;
        }
    }

    return 0;
}

void dirbuf_add(fuse_req_t req, dirbuf_t *b, const char *name, fuse_ino_t ino) {
    struct stat stbuf;
    size_t oldsize = b->size;
    b->size += fuse_add_direntry(req, NULL, 0, name, NULL, 0);
    b->p = (char *)realloc(b->p, b->size);
    memset(&stbuf, 0, sizeof(stbuf));
    stbuf.st_ino = ino;
    fuse_add_direntry(req, b->p + oldsize, b->size - oldsize, name, &stbuf, b->size);
}

void paw16_release(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi) {
    fuse_reply_err(req, ENOENT);
}

void paw16_statfs(fuse_req_t req, fuse_ino_t ino) {
    struct statvfs statfs;
    memset(&statfs, 0, sizeof(statfs));
    
    u_int16_t* FATblock = malloc(bootb->bpb_bytes_per_sec * bootb->bpb_FAT_secs);
    int freeFATentries = 0;
    
    blk_read(fd, FATblock, fat_offset / (bootb->bpb_bytes_per_sec), bootb->bpb_FAT_secs);
    
    int limit = (bootb->bpb_bytes_per_sec * bootb->bpb_FAT_secs) / sizeof(u_int16_t);
    for (int i = 0; i < limit; i++)
        if (FATblock[i] == 0) freeFATentries++;

    statfs.f_bsize = bootb->bpb_bytes_per_sec * bootb->bpb_sec_per_clust;
    statfs.f_frsize = bootb->bpb_bytes_per_sec;    
    statfs.f_blocks = (bootb->bpb_sectors == 0) ? bootb->bpb_huge_sectors : bootb->bpb_sectors;
    statfs.f_bfree = freeFATentries;
    statfs.f_bavail = freeFATentries;
    statfs.f_files = dir_entr_count;
    statfs.f_ffree = freeFATentries;
    statfs.f_favail = freeFATentries;
    statfs.f_fsid = 2137;
    statfs.f_flag = ST_RDONLY;
    statfs.f_namemax = 8;
    
    fuse_reply_statfs(req, &statfs);
}


void paw16_lookup(fuse_req_t req, fuse_ino_t parent, const char *name) {
    struct fuse_entry_param e;

    if ((u_int8_t)name[0] == SLOT_DELETED) {
        fuse_reply_err(req, ENOENT);
    }
        
    else {
        if (parent == 1) {
            for (int i = 1; i < bootb->bpb_root_dir_ents; i++) {
                if (strcmp(name, readable_root_names[i]) == 0) {
                    memset(&e, 0, sizeof(e));
                    e.ino = root_dir_entries[i].start_cluster + 1;
                    e.attr_timeout = 1.0;
                    e.entry_timeout = 1.0;
                    paw16_stat(e.ino, &e.attr);
                    fuse_reply_entry(req, &e);
                    return;
                }
            }
        }
        else {
            struct direntry tmp = dir_entries[parent - 1];
            assert(tmp.attributes & ATTR_DIRECTORY);

            char *curr_cluster;
            curr_cluster = malloc(bootb->bpb_sec_per_clust * bootb->bpb_bytes_per_sec);
            int clusterSectorOffset = data_sector_offset + (tmp.start_cluster * bootb->bpb_sec_per_clust);
            blk_read(fd, curr_cluster, clusterSectorOffset, bootb->bpb_sec_per_clust);

            int curr_cluster_nb = tmp.start_cluster;
            int done = 0;

            while (!done) {
                for (size_t i = 0; i < cluster_size; i += dir_entry_size) {
                    struct direntry curr_entry;
                    memcpy(&curr_entry, curr_cluster + i, dir_entry_size);
                    
                    if (strcmp(name, readable_names[curr_entry.start_cluster]) == 0) {
                        memset(&e, 0, sizeof(e));
                        e.ino = curr_entry.start_cluster + 1;
                        e.attr_timeout = 1.0;
                        e.entry_timeout = 1.0;
                        paw16_stat(e.ino, &e.attr);
                        fuse_reply_entry(req, &e);
                        
                        free(curr_cluster);
                        return;
                    }
                }

                done = load_next_cluster(curr_cluster, &curr_cluster_nb);
            }

            free(curr_cluster);
        }

        fuse_reply_err(req, ENOENT);
    }
}

void paw16_getattr(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info  *fi) {
    struct stat stbuf;
    memset(&stbuf, 0, sizeof(stbuf));

    if (paw16_stat(ino, &stbuf) == -1)
        fuse_reply_err(req, ENOENT);
    else
        fuse_reply_attr(req, &stbuf, 1.0);
}


void paw16_opendir(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info  *fi) {
    fuse_reply_open(req, fi);
}

void paw16_readdir(fuse_req_t req, fuse_ino_t ino, size_t size, off_t off, struct fuse_file_info  *fi) {
    dirbuf_t b;
    memset(&b, 0, sizeof(b));

    if (ino == 1) {
        for (int i = 1; i < bootb->bpb_root_dir_ents; i++)
            if ((u_int8_t)root_dir_entries[i].name[0] != SLOT_DELETED)
                dirbuf_add(req, &b, readable_root_names[i], root_dir_entries[i].start_cluster + 1);

        reply_buf_limited(req, b.p, b.size, off, size);
        free(b.p);
    }
    else {
        struct direntry tmp = dir_entries[ino - 1];
        assert(tmp.attributes & ATTR_DIRECTORY);

        char *currentCluster;
        currentCluster = malloc(bootb->bpb_sec_per_clust * bootb->bpb_bytes_per_sec);
        int clusterSectorOffset = data_sector_offset + (tmp.start_cluster * bootb->bpb_sec_per_clust);
        blk_read(fd, currentCluster, clusterSectorOffset, bootb->bpb_sec_per_clust);

        dirbuf_add(req, &b, ".", ino);
        dirbuf_add(req, &b, "..", ino);

        int curr_cluster_nb = tmp.start_cluster;
        int done = 0;

        while (!done) {
            for (size_t i = 0; i < cluster_size; i += dir_entry_size) {
                struct direntry curr_entry;
                memcpy(&curr_entry, currentCluster + i, dir_entry_size);
            
                if (curr_entry.name[0] != 0 && 
                    curr_entry.name[0] != '.' &&
                    (u_int8_t)curr_entry.name[0] != SLOT_DELETED)

                    dirbuf_add(req, &b, readable_names[curr_entry.start_cluster], curr_entry.start_cluster + 1);
            }

            done = load_next_cluster(currentCluster, &curr_cluster_nb);
        }
        
        reply_buf_limited(req, b.p, b.size, off, size);
        free(currentCluster);
        free(b.p);
    }
}

void paw16_releasedir(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info  *fi) {
    fuse_reply_err(req, ENOENT);
}

void paw16_open(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi) {
    if (ino < 2)
        fuse_reply_err(req, EISDIR);
    else if ((fi->flags & 3) != O_RDONLY)
        fuse_reply_err(req, EACCES);
    else
        fuse_reply_open(req, fi);
}

void paw16_read(fuse_req_t req, fuse_ino_t ino, size_t size, off_t off, struct fuse_file_info *fi) {
    
    struct direntry tmp = dir_entries[ino - 1];
    assert(!(tmp.attributes & ATTR_DIRECTORY));
    
    char *curr_cluster;
    curr_cluster = malloc(bootb->bpb_sec_per_clust * bootb->bpb_bytes_per_sec);
    int cluster_sector_offset = data_sector_offset + (tmp.start_cluster * bootb->bpb_sec_per_clust);
    blk_read(fd, curr_cluster, cluster_sector_offset, bootb->bpb_sec_per_clust);
    
    char* response;
    response = malloc(size);
    size_t offset = 0;  
    
    int curr_cluster_nb = tmp.start_cluster;
    int done = 0;

    for (size_t i = 0; i < size; i++) {
        if (done)
            response[i] = 0;
            
        else {
            response[i] = curr_cluster[i - offset];
            
            if (i % cluster_size == cluster_size - 1) {
                offset += cluster_size;
                done = load_next_cluster(curr_cluster, &curr_cluster_nb);
            }
        }
    }

    reply_buf_limited(req, response, size, off, size);
    free(curr_cluster);
    free(response);
}


static struct fuse_lowlevel_ops paw16_oper = {
    .lookup = paw16_lookup,
    .getattr = paw16_getattr,
    .opendir = paw16_opendir,
    .readdir = paw16_readdir,
    .releasedir = paw16_releasedir,
    .open = paw16_open,
    .read = paw16_read,
    .release = paw16_release,
    .statfs = paw16_statfs
};


int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("usage: FILESYSNAME MOUNTDIR\n");
        return EXIT_FAILURE;
      }

    char* image_name = argv[1];
    argv[1] = argv[2];
    argv[2] = NULL;
    argc--;

    fd = blk_open(image_name);
    struct boot_sector50 buf;
    blk_read(fd, &buf, 0, 1);
    bootb = &buf.bs_BPB;

    int root_offset = bootb->bpb_fats * bootb->bpb_FAT_secs + bootb->bpb_res_sectors;
    int root_size = bootb->bpb_root_dir_ents * sizeof(struct direntry);
    int root_sector_count = root_size / bootb->bpb_bytes_per_sec;

    int sector_count = (bootb->bpb_sectors == 0) ? bootb->bpb_huge_sectors : bootb->bpb_sectors;
    int cluster_count = sector_count / bootb->bpb_sec_per_clust;
    
    fat_offset = bootb->bpb_res_sectors * bootb->bpb_bytes_per_sec;
    
    data_sector_offset = root_offset + root_sector_count;
    data_sector_offset -= bootb->bpb_sec_per_clust * 2;
    
    cluster_size = bootb->bpb_bytes_per_sec * bootb->bpb_sec_per_clust;
    dir_entry_size = sizeof(struct direntry);
    
    root_dir_entries = malloc(root_sector_count*(bootb->bpb_bytes_per_sec));
    blk_read(fd, root_dir_entries, root_offset, root_sector_count);
    
    dir_entries = malloc(cluster_count * sizeof(struct direntry));
    readable_names = malloc(cluster_count * sizeof(char[13]));
    readable_root_names = malloc(bootb->bpb_root_dir_ents * sizeof(char[13]));
    
    dir_entr_count = 1;
    fill_direntries_trigger();
    
    struct fuse_args args = FUSE_ARGS_INIT(argc, argv);
    struct fuse_session *se;
    struct fuse_cmdline_opts opts;
    int ret = -1;
    
    
    if (fuse_parse_cmdline(&args, &opts) != 0)
        return 1;
    if (opts.show_help) {
        printf("usage: %s [options] <mountpoint>\n\n", argv[0]);
        fuse_cmdline_help();
        fuse_lowlevel_help();
        ret = 0;
        goto err_out1;
    } else if (opts.show_version) {
        fuse_lowlevel_version();
        ret = 0;
        goto err_out1;
    }
    
    se = fuse_session_new(&args, &paw16_oper,
            sizeof(struct fuse_lowlevel_ops), NULL);
    if (se == NULL)
        goto err_out1;
    
    if (fuse_set_signal_handlers(se) != 0)
        goto err_out2;
    
    if (fuse_session_mount(se, opts.mountpoint) != 0)
        goto err_out3;
    
    fuse_daemonize(opts.foreground);
    
    /* Block until ctrl+c or fusermount -u */
    if (opts.singlethread)
        ret = fuse_session_loop(se);
    else
        ret = fuse_session_loop_mt(se, opts.clone_fd);
    
    fuse_session_unmount(se);
    err_out3:
	fuse_remove_signal_handlers(se);
    err_out2:
	fuse_session_destroy(se);
    err_out1:
	free(opts.mountpoint);
    fuse_opt_free_args(&args);
    
    free(dir_entries);
    free(root_dir_entries);
    free(readable_names);
    free(readable_root_names);
    blk_close(fd);
    
    return ret ? 1 : 0;
}
