#ifndef _PAW16_LL_H_
#define _PAW16_LL_H_

#define BLKSIZE 512

typedef struct {
    char *p;
    size_t size;
} dirbuf_t;

struct boot_sector33 {
  u_int8_t bs_jump[3];     
  int8_t bs_oem_name[8];    
  int8_t bs_BPB[19];
  int8_t bs_drive_mumber;
  int8_t bs_boot_code[479];
  u_int8_t bs_boot_sect_sig0;
  u_int8_t bs_boot_sect_sig1;
#define BOOTSIG0 0x55
#define BOOTSIG1 0xaa
};

struct boot_sector50 {
  u_int8_t bs_jump[3];     
  int8_t bs_oem_name[8];    
  int8_t bs_BPB[25];       
  int8_t bs_ext[26];       
  int8_t bs_boot_code[448]; 
  u_int8_t bs_boot_sect_sig0;
  u_int8_t bs_boot_sect_sig1;
#define BOOTSIG0 0x55
#define BOOTSIG1 0xaa
};

struct boot_sector710 {
  u_int8_t bs_jump[3];
  int8_t bs_oem_name[8];
  int8_t bs_BPB[53];
  int8_t bs_ext[26];
  int8_t bs_boot_code[420];
  u_int8_t bs_boot_sect_sig0;
  u_int8_t bs_boot_sect_sig1;
#define BOOTSIG0 0x55
#define BOOTSIG1 0xaa
};

union boot_sector {
  struct boot_sector33 bs33;
  struct boot_sector50 bs50;
  struct boot_sector710 bs710;
};

struct bpb50 {
  u_int16_t bpb_bytes_per_sec; /* bytes per sector */
  u_int8_t bpb_sec_per_clust;  /* sectors per cluster */
  u_int16_t bpb_res_sectors;  /* number of reserved sectors */
  u_int8_t bpb_fats;         /* number of FATs */
  u_int16_t bpb_root_dir_ents; /* number of root directory entries */
  u_int16_t bpb_sectors;     /* total number of sectors */
  u_int8_t bpb_media;        /* media descriptor */
  u_int16_t bpb_FAT_secs;     /* number of sectors per FAT */
  u_int16_t bpb_sec_per_track; /* sectors per track */
  u_int16_t bpb_heads;       /* number of heads */
  u_int32_t bpb_hidden_secs;  /* # of hidden sectors */
  u_int32_t bpb_huge_sectors; /* # of sectors if bpbSectors == 0 */
} __attribute__((packed));

struct direntry {
  u_int8_t name[11];        /* filename, blank filled */
#define SLOT_EMPTY 0x00       /* slot has never been used */
#define SLOT_E5 0x05          /* the real value is 0xe5 */
#define SLOT_DELETED 0xe5     /* file in this slot deleted */
  u_int8_t attributes;      /* file attributes */
#define ATTR_NORMAL 0x00      /* normal file */
#define ATTR_READONLY 0x01    /* file is readonly */
#define ATTR_HIDDEN 0x02      /* file is hidden */
#define ATTR_SYSTEM 0x04      /* file is a system file */
#define ATTR_VOLUME 0x08      /* entry is a volume label */
#define ATTR_DIRECTORY 0x10   /* entry is a directory name */
#define ATTR_ARCHIVE 0x20     /* file is new or modified */
  u_int8_t lower_case;       /* NT VFAT lower case flags */
#define LCASE_BASE 0x08       /* filename base in lower case */
#define LCASE_EXT 0x10        /* filename extension in lower case */
  u_int8_t chundredth;      /* hundredth of seconds in CTime */
  u_int16_t ct_time;          /* create time */
  u_int16_t cd_date;          /* create date */
  u_int16_t a_date;          /* access date */
  u_int16_t high_clust;      /* high bytes of cluster number */
  u_int16_t m_time;          /* last update time */
  u_int16_t m_date;          /* last update date */
  u_int16_t start_cluster;   /* starting cluster of file */
  u_int32_t file_size;       /* size of file in bytes */
} __attribute__((packed));

#endif /* !__PAW16_LL_H_ */